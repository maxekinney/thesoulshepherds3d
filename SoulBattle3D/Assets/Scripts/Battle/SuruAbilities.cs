﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuruAbilities : MonoBehaviour
{
    public PlayerScript3D suruPlayerScript3D;
    public EnemyScript3D refEnemyScript;
    public bool enoughMp = false;
    // Start is called before the first frame update
    void Start()
    {
        suruPlayerScript3D = GameObject.Find("Suru").GetComponent<PlayerScript3D>();
        refEnemyScript= GameObject.FindGameObjectWithTag("Enemy").GetComponent<EnemyScript3D>();
    }

    // Update is called once per frame
    void Update()
    {
        //Reset checker to see if enough mp (used to determine whether animation should be played
        if(suruPlayerScript3D.PlayerTurn == false)
        {
            enoughMp = false;
        }
    }


    //Slash Abilities
    public void SoulSlash()
    {
        if (suruPlayerScript3D.mp >= 6)
        {
            enoughMp = true;
            suruPlayerScript3D.attackDamage = suruPlayerScript3D.attack + suruPlayerScript3D.magic / 2;
            Attacking();
            suruPlayerScript3D.mp = suruPlayerScript3D.mp - 6;
        }
        
    }

    //Pierce Abilities
    public void SpeedSlash()
    {
        if(suruPlayerScript3D.mp >=3)
        {
            enoughMp = true;
            suruPlayerScript3D.attackDamage = suruPlayerScript3D.attack + suruPlayerScript3D.speed / 2;
            Attacking();
            suruPlayerScript3D.mp = suruPlayerScript3D.mp - 3;
        }
        
    }

    public void SoulBlaze()
    {
        if (suruPlayerScript3D.mp >= 5)
        {
            enoughMp = true;
            suruPlayerScript3D.attackDamage = suruPlayerScript3D.magic;
            Attacking();
            suruPlayerScript3D.mp = suruPlayerScript3D.mp - 5;
        }
    }
    //calculate damage once 
    public void Attacking()
    {
        refEnemyScript.takeAttack();
    }
}
