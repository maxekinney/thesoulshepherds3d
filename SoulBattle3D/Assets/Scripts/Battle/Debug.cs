﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Debug : MonoBehaviour
{
    public Text stats;
    public Text statsE;
    public PlayerScript3D playerScript;
    public EnemyScript3D enemyScript;
    // Start is called before the first frame update
    void Start()
    {
        PlayerScript3D playerScript = GameObject.Find("Suru").GetComponent<PlayerScript3D>();
        EnemyScript3D enemyScript = GameObject.Find("Enemy").GetComponent<EnemyScript3D>();
    }

    // Update is called once per frame
    void Update()
    {
        stats.text = "HP: " + playerScript.health + "\r\n"
                        + "MP: " + playerScript.mp + "\r\n"
                           + "Attack: " + playerScript.attack + "\r\n"
                           + "Magic: " + playerScript.magic + "\r\n"
                           + "Defense: " + playerScript.defense + "\r\n"
                            + "MegicDefense: " + playerScript.magdef + "\r\n"
                           + "Speed: " + playerScript.speed;

        statsE.text = "HP: " + enemyScript.healthE + "\r\n"
                       + "MP: "+ enemyScript.mpE + "\r\n"
                           + "Attack: " + enemyScript.attackE + "\r\n"
                           + "Magic: " + enemyScript.magicE + "\r\n"
                           + "Defense: " + enemyScript.defenseE + "\r\n"
                            + "MegicDefense: " + enemyScript.magdefE + "\r\n"
                           + "Speed: " + enemyScript.speedE;
    }
}
