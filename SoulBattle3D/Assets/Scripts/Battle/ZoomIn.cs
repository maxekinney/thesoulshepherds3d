﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoomIn : MonoBehaviour
{
    public Transform Player;
    public float zoom;
    public Vector3 OriginPoint;
    public Vector3 ZoomInPoint;
    public bool ZoomCheck;
    // Start is called before the first frame update
    void Start()
    {
        OriginPoint = gameObject.transform.position;
        ZoomInPoint = new Vector3(0, 1, -3);
        ZoomCheck = false;
    }

    // Update is called once per frame
    void Update()
    {
        //zoom = Input.GetAxis("CameraZoom");

        

        if(Input.GetKeyDown(KeyCode.Joystick1Button9))
        {
            UnityEngine.Debug.Log("Zoom");
           if(ZoomCheck == false)
            {
                transform.position = ZoomInPoint;
                ZoomCheck = true;
                UnityEngine.Debug.Log("Zoom2");
                return;
            }
           if(ZoomCheck == true)
            {
                transform.position = OriginPoint;
                ZoomCheck = false;
            }
        }
        
    }
}
