﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    public GameObject Suru;
    public PlayerScript3D SuruScript;

    public EnemyScript3D EnemScript;
    //PlayerUI
    public Text PlayerHP;
    public Text PlayerMP;

    //General Text
    public Text BattleInfo;
    //EnemyUI
    public Text EnemyHP;


    // Start is called before the first frame update
    void Start()
    {
        SuruScript = Suru.GetComponent<PlayerScript3D>();
        EnemyScript3D EnemScript = GameObject.FindGameObjectWithTag("Enemy").GetComponent<EnemyScript3D>();
    }

    // Update is called once per frame
    void Update()
    {
        PlayerHP.text = "HP: " + SuruScript.health;
        PlayerMP.text = "MP: " + SuruScript.mp;
        EnemyHP.text = "HP: " + EnemScript.healthE;
    }

    public void EnemyAttackText()
    {
        if (SuruScript.Defending == true)
        {
            BattleInfo.text = "Enemy Dealt " + ((EnemScript.attackDamageE - SuruScript.defense) / 2) + " Damage";
        }
        if (SuruScript.Defending == false)
        {
            BattleInfo.text = "Enemy Dealt " + (EnemScript.attackDamageE - SuruScript.defense) + " Damage";
        }

        
    }

    public void PlayerAttackText()
    {
        BattleInfo.text = "Enemy Takes " + SuruScript.attackDamage + " Damage";
    }
}
