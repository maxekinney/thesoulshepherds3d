﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PlayerScript3D : MonoBehaviour
{
    public Vector3 PlayerPosition;
      public float  playerSpeedH;
    public float playerSpeedV;
    //public float speed;
    public GameObject Player;
    //Rotation

    public float movespeed;

    //NEW MOVEMENT
    public Transform camPivot;
    float heading = 0;
    float headingx = 0;
    Vector2 input;
    public Transform cam;
    //END OF NEW MOVEMENT


    public EventSystem AbilityEventSystem;

    //Original Values
    public int originHealth;
    public int originMp;
    public int originAttack;
    public int originMagic;
    public int originDefense;
    public int originMagDef;
    public int originSpeed;

    //Values to be calculated
    [HideInInspector]
    public int health;
    [HideInInspector]
    public int mp;
    [HideInInspector]
    public int attack;
    [HideInInspector]
    public int magic;
    [HideInInspector]
    public int defense;
    [HideInInspector]
    public int magdef;
    [HideInInspector]
    public int speed;

    //damage value calculated in SuruAbilities Script
    [HideInInspector]
    public int attackDamage;

    //Stat Boosts
    public int boostAtk;
    public int boostMag;
    public int boostDef;
    public int boostMDef;
    public int boostSpd;

    //determine whether or not attacking (Substitute for triggers)
    public bool Attacking = false;

    //Determine if Defending
    public bool Defending = false;
    
    //Modes
    public string Mode;

    //Hitboxes
    public GameObject SlashAttack;
    public GameObject PierceAttack;
    public GameObject MagicAttack;

    //UI
    public SpriteRenderer ModeIcon;
    public Sprite SlashIcon;
    public Sprite PierceIcon;
    public Sprite MagicIcon;

   
    //Targeted Enemy
    public GameObject Target;

    //UI elements
    public GameObject SlashAbilityMenu;
    public GameObject PierceAbilityMenu;
    public GameObject MagicAbilityMenu;

    public EnemyScript3D refEnemyScript;

    //Animation Script
    public SuruAnim refSuruAnim;

    //PlayerTurn Checker
    public bool PlayerTurn;
    //attacktimer

    //Weapons
    public GameObject SlashWeapon;
    public GameObject[] PierceWeapon;
    public GameObject MagicWeapon;

    //have player look in direction camera is facing
    private float y;
    private Vector3 rotateValue;
    //camera
    public GameObject mainCamera;
    //in order to have "homing attacks" we must seek the enemy's transform position, and emit the spell with force, with vector (enemy position)
    // above must be in Update method, (the tracking of enemy location)

    // Start is called before the first frame update
    void Start()
    {
        ModeIcon = ModeIcon.GetComponent<SpriteRenderer>();
        mainCamera = GameObject.Find("CameraTracker");
        health = originHealth;
        mp = originMp;
        defense = originDefense;
        magic = originMagic;
        speed = originSpeed;
        attack = originAttack;
        Mode = "Slash";

        refEnemyScript = GameObject.FindGameObjectWithTag("Enemy").GetComponent<EnemyScript3D>();

        //find Animation Script
        refSuruAnim = GameObject.FindGameObjectWithTag("Animated").GetComponent<SuruAnim>();

        //find ability menu and deactivate
        
        SlashAbilityMenu.SetActive(false);
        
        PierceAbilityMenu.SetActive(false);

        PlayerTurn = true;

        //Set Weapons to false
        //SlashWeapon.SetActive(false);
        for (int i = 0; i < PierceWeapon.Length; i++)
        {
            PierceWeapon[i].SetActive(false);
        }
        
        MagicWeapon.SetActive(false);
    }

    // Update is called once per frame
    void Update()
{

        //NEW MOVEMENT
        heading += Input.GetAxis("HorizontalCamera") * Time.deltaTime * 180;
        headingx += Input.GetAxis("VerticalCamera") * Time.deltaTime * 180;
        //Set maximum ammount you can rotate downwards
        if (headingx > 20)
        {
            headingx = 20;
        }
        camPivot.rotation = Quaternion.Euler(headingx*-1, heading, 0);

        input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        input = Vector2.ClampMagnitude(input, 1);


        Vector3 camF = cam.forward;
        Vector3 camR = cam.right;

        camF.y = 0;
        camR.y = 0;

        camF = camF.normalized;
        camR = camR.normalized;
        //transform.position += new Vector3(input.x, 0, input.y)*Time.deltaTime;
        transform.position += (camF * input.y + camR * input.x) * Time.deltaTime * speed;

        //END OF NEW MOVEMENT

        //Movement
        //PlayerPosition = gameObject.transform.position;
   // playerSpeedH = Input.GetAxisRaw("Horizontal") * speed;
    //transform.Translate(playerSpeedH * Time.deltaTime, 0f, 0f);

   // playerSpeedV = Input.GetAxisRaw("Vertical") * speed;
    //transform.Translate(0f, 0f, playerSpeedV*Time.deltaTime);//makes player run forward

    //Check Camera Rotation
   // y = Input.GetAxis("HorizontalCamera");
    //rotateValue = new Vector3(0, y * -1, 0);
   // transform.eulerAngles = transform.eulerAngles - rotateValue;

    //transform.Translate(playerSpeedH * Time.deltaTime, 0f, playerSpeedV * Time.deltaTime);
    //if (Input.GetAxis("Vertical") != 0 || Input.GetAxis("Horizontal") != 0)
   // {
        //player only moves in the direction the character is facing: character rotates.    
    //    transform.Translate(0, 0, (speed*Time.deltaTime));

        //rotate character
    //    Vector3 lookDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
    //    Quaternion lookRotation = Quaternion.LookRotation(lookDirection, transform.up);
    //    float step = 1 * Time.deltaTime;
    //    transform.rotation = Quaternion.RotateTowards(lookRotation, transform.rotation, step);
   // }


        //change mode right
        if (Input.GetKeyDown(KeyCode.Joystick1Button5)||Input.GetKeyDown(KeyCode.E))
        {
            UnityEngine.Debug.Log("check");
            closeAbility();
            ChangeModeR();
        }
        //change mode left
        if (Input.GetKeyDown(KeyCode.Q)||Input.GetKeyDown(KeyCode.Joystick1Button4))
        {
            closeAbility();
            ChangeModeL();
        }
        //if attack button pressed
        if(Input.GetKeyDown(KeyCode.Joystick1Button1))
        {

            UnityEngine.Debug.Log("B");
            //Have to reference script from here, as the animation script is slower somehow.
            //refSuruAnim.anim.SetTrigger("Attack");
            //StartCoroutine(attackTimer());
            if (PlayerTurn == true)
            {
                Attack();
                //PlayerTurn = false;
                return;
            }
            
        }

        
            //ability menu
            if (Input.GetKeyDown(KeyCode.Joystick1Button3))
        {
           if(PlayerTurn == true)
            {
                openAbility();
            }
        }
        if(PlayerTurn == true)
        {
            Defending = false;
        }
        if (Input.GetKeyDown(KeyCode.Joystick1Button2))
        {
            Defend();
            
            
        }
        UnityEngine.Debug.Log(Defending);
    }

    public void takeAttackP()
    {
        if (Defending == false)
        { health = health - (refEnemyScript.attackDamageE - defense); }
        if (Defending == true)
        {
            health = health - (refEnemyScript.attackDamageE - defense)/2;
            Defending = false;
        }
        PlayerTurn = true;
    }

    //Cycle mode once right
    public void ChangeModeR()
    {
        
        resetMode();
        if (Mode == "Magic")
        {
            Mode = "Slash";
            changeStatSlash();
            SlashWeapon.SetActive(true);
            MagicWeapon.SetActive(false);
            ModeIcon.sprite = SlashIcon;
            return;
        }
        else
        {
            resetMode();
            SlashWeapon.SetActive(false);
        }

        if (Mode == "Pierce")
        {
            Mode = "Magic";
            changeStatMagic();
            for (int i = 0; i < PierceWeapon.Length; i++)
            {
                PierceWeapon[i].SetActive(false);
            }
            MagicWeapon.SetActive(true);
            ModeIcon.sprite = MagicIcon;
            return;
        }
        else
        {
            resetMode();
            MagicWeapon.SetActive(false);
        }
        
        if (Mode == "Slash")
        {
            Mode = "Pierce";
            changeStatPierce();
            for (int i = 0; i < PierceWeapon.Length; i++)
            {
                PierceWeapon[i].SetActive(true);
            }
            ModeIcon.sprite = PierceIcon;

            return;
        }
        else
        {
            resetMode();
            UnityEngine.Debug.Log("Change");
            for (int i = 0; i < PierceWeapon.Length; i++)
            {
                PierceWeapon[i].SetActive(false);
            }
            
        }
    }
    //Cycle modes once left
    public void ChangeModeL()
    {
        resetMode();
        if (Mode == "Magic")
        {
            MagicWeapon.SetActive(false);

            Mode = "Pierce";
            changeStatPierce();
            for (int i = 0; i < PierceWeapon.Length; i++)
            {
                PierceWeapon[i].SetActive(true);
            }
            ModeIcon.sprite = PierceIcon;

            return;
        }
        else
        {
            resetMode();
            //SlashWeapon.SetActive(false);

            for (int i = 0; i < PierceWeapon.Length; i++)
            {
                PierceWeapon[i].SetActive(false);
            }
        }

        if (Mode == "Pierce")
        {
            

            Mode = "Slash";
            changeStatSlash();
            for (int i = 0; i < PierceWeapon.Length; i++)
            {
                PierceWeapon[i].SetActive(false);
            }
            SlashWeapon.SetActive(true);
            ModeIcon.sprite = SlashIcon;
            return;
        }
        else
        {
            resetMode();
            SlashWeapon.SetActive(false);
        }

        if (Mode == "Slash")
        {
            Mode = "Magic";
            changeStatMagic();
            MagicWeapon.SetActive(true);
            ModeIcon.sprite = MagicIcon;

            return;
        }
        else
        {
            resetMode();
            UnityEngine.Debug.Log("Change");
            MagicWeapon.SetActive(false);

        }
    }
    //attack without IEnumorator
    public void Attack()
    {
        Attacking = true;
        //Attack Damage is base attack
        refSuruAnim.anim.SetTrigger("Attack");

        if (Mode == "Slash")
        {
            
            Attacking = true;
            attackDamage = attack;
            SlashAttack.SetActive(true);
            
            SlashAttack.SetActive(false);
            
            refEnemyScript.takeAttack();

            
        }

        if(Mode == "Pierce")
        {
            Attacking = true;
            attackDamage = speed;
            

            refEnemyScript.takeAttack();
        }

        //if(Mode== "Magic")
        //{
         //   Attacking = true;
         //   attackDamage = magic;


          //  refEnemyScript.takeAttack();

       // }
        Attacking = false;

        //End Player Turn
        PlayerTurn = false;
    }
    //Dodge Mechanic
    public void Dodge()
    {
        //movement

        //invulnerability
        //attack needs tag, player needs tag for invulnerable/vulnerable status.
    }

    public void Defend()
    {
        if(PlayerTurn == true)
        {
            Defending = true;
            PlayerTurn = false;
        }
    }






    //mode stats

    public void resetMode()
    {
        //health = originHealth;
        attack = originAttack;
        magic = originMagic;
        defense = originDefense;
        speed = originSpeed;
    }
    
    
    public void changeStatSlash()
    {
        //If object is Suru
        if(gameObject.tag == "Suru")
        {
            attack = originAttack + boostAtk;
        }

        //if object is Duke
        if(gameObject.tag == "Duke")
        {
            defense = originDefense + boostDef;
        }

        //if object Lydia
        if(gameObject.tag == "Lydia")
        {
            attack = originAttack + boostAtk;
        }
    }

    public void changeStatPierce()
    {
        //if object Suru
        if (gameObject.tag == "Suru")
        {
            speed = originSpeed + boostSpd;
            defense = originDefense - 2;
        }

        //if object is Duke
        if (gameObject.tag == "Duke")
        {
            speed = originSpeed + boostSpd;
            attack = originAttack + boostAtk;
            defense = originDefense - boostDef;
        }

        //if object Lydia
        if (gameObject.tag == "Lydia")
        {
            speed = originSpeed + boostSpd;
        }
    }

    public void changeStatMagic()
    {
        //if gameobject suru
        if (gameObject.tag == "Suru")
        {
            magic = originMagic + boostMag;
        }

        //if object is Duke
        if (gameObject.tag == "Duke")
        {
            speed = originSpeed + boostSpd;
            magic = originMagic + boostMag;
        }

        //if object Lydia
        if (gameObject.tag == "Lydia")
        {
            magic = originMagic + boostMag;
        }
    }

    //end of mode stats




    //open slashability Menu
    public void openAbility()
    {
        if (Mode == "Slash")
        {
            SlashAbilityMenu.SetActive(true);
            AbilityEventSystem.SetSelectedGameObject(GameObject.FindGameObjectWithTag("MenuStart"));
        }
        if(Mode == "Pierce")
        {
            PierceAbilityMenu.SetActive(true);
            AbilityEventSystem.SetSelectedGameObject(GameObject.FindGameObjectWithTag("MenuStart"));
            
        }
        if(Mode == "Magic")
        {
            MagicAbilityMenu.SetActive(true);
            AbilityEventSystem.SetSelectedGameObject(GameObject.FindGameObjectWithTag("MenuStart"));
        }
    }
    //close slashability Menu
    public void closeAbility()
    {
        SlashAbilityMenu.SetActive(false);
        PierceAbilityMenu.SetActive(false);
        MagicAbilityMenu.SetActive(false);
    }
    //open pierceability Menu
}
