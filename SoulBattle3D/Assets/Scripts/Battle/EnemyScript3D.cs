﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript3D : MonoBehaviour
{
    //enemy mesh
    public GameObject enemMesh;
    //Enemy Animator
    public Animator enemAnim;
    //Enemy Stats
    public int originHealthE;
    public int originMpE;
    public int originAttackE;
    public int originMagicE;
    public int originDefenseE;
    public int originMagDefenseE;
    public int originSpeedE;

    [HideInInspector]
    public int healthE;
    [HideInInspector]
    public int mpE;
    [HideInInspector]
    public int attackE;
    [HideInInspector]
    public int magicE;
    [HideInInspector]
    public int defenseE;
    [HideInInspector]
    public int magdefE;
    [HideInInspector]
    public int speedE;

    //Enemy Attack Collider
    public GameObject EnemyAttack;

    //calculated damage
    public int attackDamageE;
    //Reference to PlayerScripts
    public PlayerScript3D refSuru;
    public PlayerScript3D refLydia;
    public PlayerScript3D refDuke;

    //reference to menu script
    public Menu refMenu;

    public float attackTimerE;
    // Start is called before the first frame update
    void Start()
    {
        refSuru = GameObject.Find("Suru").GetComponent<PlayerScript3D>();
        refMenu = GameObject.Find("MenuManager").GetComponent<Menu>();
        healthE = originHealthE;
        mpE = originMpE;
        defenseE = originDefenseE;
        magicE = originMagicE;
        speedE = originSpeedE;
        attackE = originAttackE;
        enemAnim = enemMesh.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (refSuru.Attacking == true)
        {
            //UnityEngine.Debug.Log("playerturn");
            //healthE = healthE - (refSuru.attackDamage - defenseE);
            takeAttack();
            
        }
        if(refSuru.PlayerTurn == false)
        {
            if (attackTimerE <= 0f) attackTimerE = 2f; // wait 1 second before attacking

            //wait for attack
            if (attackTimerE > 0f)
            {
                refMenu.PlayerAttackText();
                //Put player text damage here
                attackTimerE -= Time.deltaTime;
                if (attackTimerE <= 0f)
                {
                    //Put enemy damage dealt to player here text
                    
                    AttackE();
                    refMenu.EnemyAttackText();
                }
            }
        }
        
    }

    public void OnTriggerEnter(Collider other)
    {
        //if trigger is Attack object
        if (other.gameObject.tag == "Attack")
        {
            
            //Attacked by Sword mode Suru
            if(other.gameObject.name == "Sword Attack")
            {
                
                healthE = healthE - (refSuru.attackDamage - defenseE);
            }
            
            
        }
    }



    public void takeAttack()
    {

        healthE = healthE - (refSuru.attackDamage - defenseE);
        refSuru.PlayerTurn = false;
        
    }

    public void AttackE()
    {
        //play attack animation
        enemAnim.SetTrigger("Attack");
        attackDamageE = originAttackE;
        refSuru.takeAttackP();
        return;

        // refSuru.PlayerTurn = true;


    }

    
}
