﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour
{
    public PlayerScript3D refPlayer;
    public EnemyScript3D refEnemy;

    public GameObject WinMenu;
    // Start is called before the first frame update
    void Start()
    {
        WinMenu.SetActive(false);
        refPlayer = GameObject.Find("Suru").GetComponent<PlayerScript3D>();
        refEnemy = GameObject.Find("Enemy").GetComponent<EnemyScript3D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (refEnemy.healthE <= 0)
        {
            WinMenu.SetActive(true);
        }

    }
}
