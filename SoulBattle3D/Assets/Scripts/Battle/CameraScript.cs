﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour

{
    private float x;
    private float y;
    private Vector3 rotateValue;
    //SuruScript
    public PlayerScript3D refSuru;
    public GameObject Suru;
    public Vector3 suruLocation;
    private void Start()
    {
        Suru = GameObject.Find("Suru");
        refSuru = GameObject.Find("Suru").GetComponent<PlayerScript3D>();
    }
    void Update()
    {
        transform.position = Suru.transform.position;
        y = Input.GetAxis("HorizontalCamera");
        x = Input.GetAxis("VerticalCamera");
        UnityEngine.Debug.Log(x + ":" + y);
        rotateValue = new Vector3(x, y * -1, 0);
        transform.eulerAngles = transform.eulerAngles - rotateValue;
    }
}

