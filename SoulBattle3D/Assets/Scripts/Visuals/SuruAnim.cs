﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SuruAnim : MonoBehaviour
{
    public GameObject suru;
    public PlayerScript3D refSuru;
    public float Rotation;
    public float rotationSpeed = 2;
    public Animator anim;
    public RuntimeAnimatorController[] anim1;

    //NEW ROTATION
    public Transform camPivot;
    float heading = 0;
    Vector2 input;
    public Transform cam;
    //END OF NEW ROTATION

    //have player look in direction camera is facing
    private float y;
    private Vector3 rotateValue;
    public Transform SuruFacing;
    private float SuruRotation;
    public GameObject Suru;
    public Transform mainCamera;
    public SuruAbilities refAbility;

    float headingx = 0;

    // Start is called before the first frame update
    void Start()
    {
        mainCamera = GameObject.Find("CameraTracker").GetComponent<Transform>(); ;
        Suru = GameObject.Find("Suru");
        refSuru = GameObject.Find("Suru").GetComponent<PlayerScript3D>();
        anim = gameObject.GetComponent<Animator>();
        SuruFacing = GameObject.Find("Suru").GetComponent<Transform>();
        refAbility = GameObject.Find("Suru").GetComponent<SuruAbilities>();

    }

    // Update is called once per frame
    void Update()
    {
        //track look rotation of camera
        // y = Input.GetAxis("HorizontalCamera");
        

        // rotateValue = new Vector3(0, y * -1, 0);
        //transform.eulerAngles = transform.eulerAngles - rotateValue;
        SuruFacing = GameObject.Find("Suru").GetComponent<Transform>();
        SuruRotation = SuruFacing.rotation.y;
        //UnityEngine.Debug.Log(suru.transform.up);
        // y = Input.GetAxis("HorizontalCamera");

        //rotateValue = new Vector3(0, y * -1, 0);
        //NEW ROTATION
        heading += Input.GetAxis("HorizontalCamera") * Time.deltaTime * 180;
        headingx += Input.GetAxis("VerticalCamera") * Time.deltaTime * 180;
        //Set maximum ammount you can rotate downwards
        if (headingx > 20)
        {
            headingx = 20;
        }
        camPivot.rotation = Quaternion.Euler(headingx*-1, heading, 0);

        input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        input = Vector2.ClampMagnitude(input, 1);


        Vector3 camF = cam.forward;
        Vector3 camR = cam.right;

        camF.y = 0;
        camR.y = 0;

        camF = camF.normalized;
        camR = camR.normalized;
        //transform.position += new Vector3(input.x, 0, input.y)*Time.deltaTime;
        //transform.position += (camF * input.y + camR * input.x) * Time.deltaTime * 5;
        //END OF NEW ROTATION
       // Vector3 targetDirection = new Vector3(horizontal, 0f, vertical);
       // targetDirection = Camera.main.transform.TransformDirection(targetDirection);
       // targetDirection.y = 0.0f;
        // transform.eulerAngles = transform.eulerAngles - rotateValue;
        if (Input.GetAxis("Vertical") != 0 || Input.GetAxis("Horizontal") != 0)
        {
            transform.forward = camF;
            //direction of movement input
            //float inputZ = Input.GetAxis("Vertical");
            // float inputX = Input.GetAxis("Horizontal");

            Vector3 lookDirection = new Vector3(input.x, 0, input.y);
            //rotate to direction of pressing stick
            lookDirection = Camera.main.transform.TransformDirection(lookDirection);
            lookDirection.y = 0.0f;
           // Vector3 lookDirection = new Vector3(inputX, 0, inputZ);
            //UnityEngine.Debug.Log(Suru.transform.forward);
            
            ///Vector3 worldInputMovement = transform.TransformDirection(lookDirection.normalized);
            //UnityEngine.Debug.Log(transform.forward);
            Quaternion lookRotation = Quaternion.LookRotation(lookDirection, transform.up);
           float step = rotationSpeed * Time.deltaTime;
         transform.rotation = Quaternion.RotateTowards(lookRotation, transform.rotation, step);
            


            

            

            anim.SetBool("Walking", true);

          

        }
        else
        {
            anim.SetBool("Walking", false);
        }

        //change mode
        if (Input.GetKeyDown(KeyCode.Joystick1Button5) || Input.GetKeyDown(KeyCode.E))
        {
            //when change to Slash
            if (refSuru.Mode == "Magic")
            {
                anim.runtimeAnimatorController = anim1[0];
                
                return;
            }
            else
            {
                
            }
            //When Change to Magic
            if (refSuru.Mode == "Pierce")
            {
                anim.runtimeAnimatorController = anim1[2];
                return;
            }
            else
            {
                
            }
            //When Change To Pierce
            if (refSuru.Mode == "Slash")
            {
                anim.runtimeAnimatorController = anim1[1];
                return;
            }
            else
            {
                
            }

        }
        if (Input.GetKeyDown(KeyCode.Joystick1Button1))
        {
            //AttackTimer doesnt seem to work.
            //StartCoroutine(attackTimerE());

            if (refSuru.PlayerTurn == true)
            {
            anim.SetTrigger("Attack");
            UnityEngine.Debug.Log("Attack");

            }


        }
        //change mode left
        if (Input.GetKeyDown(KeyCode.Q) || Input.GetKeyDown(KeyCode.Joystick1Button4))
        {
            //when change to Slash
            if (refSuru.Mode == "Magic")
            {
                anim.runtimeAnimatorController = anim1[1];

                return;
            }
            else
            {

            }
            //When Change to Magic
            if (refSuru.Mode == "Pierce")
            {
                anim.runtimeAnimatorController = anim1[0];
                return;
            }
            else
            {

            }
            //When Change To Pierce
            if (refSuru.Mode == "Slash")
            {
                anim.runtimeAnimatorController = anim1[2];
                return;
            }
            else
            {

            }
        }
        //if attack button pressed
        
        
    }
    private void FixedUpdate()
    {
        
    }
    public void Ability1()
    {
        if (refAbility.enoughMp == true)
        {
            anim = gameObject.GetComponent<Animator>();
            anim.SetTrigger("Ability1");
        }
    }
}
