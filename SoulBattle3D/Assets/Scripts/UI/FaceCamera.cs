﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceCamera : MonoBehaviour
{
    public Transform target;
    // Start is called before the first frame update
    void Start()
    {
        target = GameObject.Find("CameraTracker").GetComponent<Transform>();
    }

   

    void Update()
    {
        if (target != null)
            transform.rotation = target.rotation;
    }
}
