﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProximityActivate : MonoBehaviour
{
    public GameObject Icon;
    public GameObject Player;
    // Start is called before the first frame update
    void Start()
    {
        Icon.SetActive(false);
        Player = GameObject.Find("Suru");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            Icon.SetActive(true);
            UnityEngine.Debug.Log("Collision");
        }
    }
    private void OnTriggerExit(Collider other)
    {
        Icon.SetActive(false);
    }
}
