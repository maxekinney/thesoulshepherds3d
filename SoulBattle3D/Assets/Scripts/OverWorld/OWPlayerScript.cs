﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OWPlayerScript : MonoBehaviour
{
    public Vector3 PlayerPosition;
    public float playerSpeedH;
    public float playerSpeedV;
    //public float speed;
    public GameObject Player;
    //Rotation
    public float originspeed;
    public float movespeed;
    public float runspeed;

    //NEW MOVEMENT
    public Transform camPivot;
    float heading = 0;
    Vector2 input;
    public Transform cam;
    //END OF NEW MOVEMENT
    //Run Trigger
    private bool Running;
    private float y;
    private Vector3 rotateValue;
    //camera
    public GameObject mainCamera;

    float headingx = 0;
    // Start is called before the first frame update
    void Start()
    {
        mainCamera = GameObject.Find("CameraTracker");
        Running = false;
        movespeed = originspeed;
    }

    // Update is called once per frame
    void Update()
    {
        heading += Input.GetAxis("HorizontalCamera") * Time.deltaTime * 180;
        headingx += Input.GetAxis("VerticalCamera") * Time.deltaTime * 180;
        //Set maximum ammount you can rotate downwards
        if (headingx > 20)
        {
            headingx = 20;
        }
        camPivot.rotation = Quaternion.Euler(headingx*-1, heading, 0);

        input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        input = Vector2.ClampMagnitude(input, 1);


        Vector3 camF = cam.forward;
        Vector3 camR = cam.right;

        camF.y = 0;
        camR.y = 0;

        camF = camF.normalized;
        camR = camR.normalized;
        //transform.position += new Vector3(input.x, 0, input.y)*Time.deltaTime;
        transform.position += (camF * input.y + camR * input.x) * Time.deltaTime * movespeed;


        if(Input.GetKeyDown(KeyCode.Joystick1Button8))
        {
            if(Running == false)
            {
                movespeed = movespeed + runspeed;
                Running = true;
                return;
            }
            else
            {
                movespeed = originspeed;
                Running = false;
                return;
            }
        }

    }
}
