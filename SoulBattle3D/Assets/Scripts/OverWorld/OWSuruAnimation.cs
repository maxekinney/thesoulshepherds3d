﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OWSuruAnimation : MonoBehaviour
{
    public float Rotation;
    public float rotationSpeed = 2;
    public Animator anim;

    //Rotation
    public Transform camPivot;
    float heading = 0;
    Vector2 input;
    public Transform cam;
    public Transform mainCamera;
    public Transform SuruFacing;
    private float SuruRotation;
    float headingx = 0;

    public bool Running;
    // Start is called before the first frame update
    void Start()
    {
        mainCamera = GameObject.Find("CameraTracker").GetComponent<Transform>();
        anim = gameObject.GetComponent<Animator>();
        SuruFacing = GameObject.Find("Suru").GetComponent<Transform>();
        Running = false;
    }

    // Update is called once per frame
    void Update()
    {
        SuruFacing = GameObject.Find("Suru").GetComponent<Transform>();
        SuruRotation = SuruFacing.rotation.y;

        heading += Input.GetAxis("HorizontalCamera") * Time.deltaTime * 180;
        headingx += Input.GetAxis("VerticalCamera") * Time.deltaTime * 180;
        //Set maximum ammount you can rotate downwards
        if (headingx > 20)
        {
            headingx = 20;
        }
        camPivot.rotation = Quaternion.Euler(headingx*-1, heading, 0);

        input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        input = Vector2.ClampMagnitude(input, 1);


        Vector3 camF = cam.forward;
        Vector3 camR = cam.right;

        camF.y = 0;
        camR.y = 0;

        camF = camF.normalized;
        camR = camR.normalized;

        if (Input.GetAxis("Vertical") != 0 || Input.GetAxis("Horizontal") != 0)
        {
            transform.forward = camF;
            //direction of movement input
            //float inputZ = Input.GetAxis("Vertical");
            // float inputX = Input.GetAxis("Horizontal");

            Vector3 lookDirection = new Vector3(input.x, 0, input.y);
            //rotate to direction of pressing stick
            lookDirection = Camera.main.transform.TransformDirection(lookDirection);
            lookDirection.y = 0.0f;
            // Vector3 lookDirection = new Vector3(inputX, 0, inputZ);
            //UnityEngine.Debug.Log(Suru.transform.forward);

            ///Vector3 worldInputMovement = transform.TransformDirection(lookDirection.normalized);
            //UnityEngine.Debug.Log(transform.forward);
            Quaternion lookRotation = Quaternion.LookRotation(lookDirection, transform.up);
            float step = rotationSpeed * Time.deltaTime;
            transform.rotation = Quaternion.RotateTowards(lookRotation, transform.rotation, step);







            anim.SetBool("Move", true);



        }
        else
        {
            anim.SetBool("Move", false);
        }
        //run animation
        if (Input.GetKeyDown(KeyCode.Joystick1Button8))
        {
            if (Running == false)
            {
                anim.SetBool("Running", true);
                Running = true;
                return;
            }
            else
            {
                anim.SetBool("Running", false);
                Running = false;
                return;
            }
        }
    }
}
